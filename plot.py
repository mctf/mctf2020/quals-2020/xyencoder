import matplotlib.pyplot as plt
import numpy as np

X = np.genfromtxt('train', delimiter=',')[1:]

ax = plt.subplot(1, 2, 1)

x_min, x_max = X[:, 1].min() - 1, X[:, 1].max() + 1
y_min, y_max = X[:, 2].min() - 1, X[:, 2].max() + 1

ax.set_xlim((x_min, x_max))
ax.set_ylim((y_min, y_max))

for l, x, y in X:
    ax.plot(x, y, 'o', color='red' if l%2 else 'blue')

X = np.genfromtxt('flag', delimiter=',')[1:]

ax = plt.subplot(1, 2, 2)

x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1

ax.set_xlim((x_min, x_max))
ax.set_ylim((y_min, y_max))

for x, y in X:
    ax.plot(x, y, 'o', color='black')

plt.show()
