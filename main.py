import numpy as np

FLAG = input('flag: ')

circles = []
CIRCLES_COUNT = 32
SIZE = CIRCLES_COUNT
while len(circles) < CIRCLES_COUNT:
    x, y = np.random.uniform(-SIZE, SIZE, 2)
    r = np.random.uniform(SIZE/8, SIZE/4)
    if all([np.linalg.norm([x-x1, y-y1]) > r + r1 for x1, y1, r1 in circles]):
        circles.append([x, y, r])

DOTS_COUNT = CIRCLES_COUNT*20

def encode(i):
    x, y, mr = circles[np.random.randint(0, CIRCLES_COUNT / 2) * 2 + i%2]
    r = np.random.uniform(0, mr)
    d = np.random.uniform(0, 2*np.pi)
    return [x+np.cos(d)*r, y+np.sin(d)*r]

with open('train', 'w') as f:
    f.write('label, x, y\n')
    for i in range(DOTS_COUNT):
        f.write(','.join(map(str, [i%2, *encode(i)])) + '\n')

with open('flag', 'w') as f:
    f.write('x, y\n')
    key = ''.join(format(ord(x), '08b') for x in FLAG)
    for i in key:
        f.write(','.join(map(str, encode(int(i)))) + '\n')




